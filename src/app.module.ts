import { Module } from '@nestjs/common';
import { DbModule } from './db/db.module';
import { EnvModule } from './env/env.module';
import { GraphQLModule } from '@nestjs/graphql';
import { ProjectsModule } from './modules/projects/projects.module';
import { AboutModule } from './modules/about/about.module';
import { ServiceModule } from './modules/service/service.module';
import { ContactService } from './modules/contact/contact.service';
import { ContactResolver } from './modules/contact/contact.resolver';
import { ContactModule } from './modules/contact/contact.module';
import { BlogModule } from './modules/blog/blog.module';
import { MailerModule } from '@nestjs-modules/mailer';

@Module({
  imports: [
    DbModule.forRoot(),
    EnvModule.register({ folder: './' }),
    GraphQLModule.forRoot({
      autoSchemaFile: 'src/schema.gql',
      debug: process.env.NODE_ENV === 'development',
      sortSchema: true,
      playground: true,
      introspection: true,
    }),
    MailerModule.forRoot({
      transport: {
        host: 'smtp.gmail.com',
        port: '465',
        secure: true,
        auth: {
          user: 'your_gmail_email',
          pass: 'your_gmail_app_password',
        },
      },
    }),
    ProjectsModule,
    AboutModule,
    ServiceModule,
    ContactModule,
    BlogModule,
  ],
  controllers: [],
  providers: [ContactService, ContactResolver],
})
export class AppModule {}
