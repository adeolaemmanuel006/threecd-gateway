class Utils {
  Date() {
    const day = [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday",
    ];
    const year = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    const date = new Date();
    const dayNo = date.getDay();
    const yearNO = date.getFullYear();
    const month = date.getMonth();
    return {
      date,
      dayNo,
      yearNO,
      month,
      dateIOS: date.toISOString(),
      monthCat: `${dayNo}|${month}|${yearNO}`,
      monthString: year[month],
    };
  }
}

export default new Utils();
