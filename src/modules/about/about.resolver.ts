import { Args, Resolver, Query, Mutation } from '@nestjs/graphql';
import { AboutService } from './about.service';
import { About } from './dto/about.dto';
import { TeamDto } from './dto/team.dto';
import { AboutEntity } from './entities/about.entity';
import { TeamEntity } from './entities/team.entity';

@Resolver()
export class AboutResolver {
  constructor(private aboutService: AboutService) {}

  @Query(() => AboutEntity, { description: 'Get about data' })
  getAbout() {
    return this.aboutService.getAbout();
  }

  @Query(() => [TeamEntity], { description: 'Get team data' })
  getTeam() {
    return this.aboutService.getTeam();
  }

  @Mutation(() => AboutEntity, { description: 'Set About' })
  setAbout(@Args('about') about: About) {
    return this.aboutService.setAbout(about);
  }

  @Mutation(() => TeamEntity, { description: 'Add Team members' })
  addTeamMember(@Args('teamInput') teamInput: TeamDto) {
    return this.aboutService.addTeamMember(teamInput);
  }

  @Mutation(() => TeamEntity, { description: 'Edit Team members' })
  editTeamMember(@Args('teamInput') teamInput: TeamDto): Promise<TeamDto> {
    return this.aboutService.editTeam(teamInput);
  }
}
