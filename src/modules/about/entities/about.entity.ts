import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class AboutEntity {
  @Field()
  summary?: string;
  @Field()
  image?: string;
  @Field({ nullable: true })
  created_at?: string;
  @Field({ nullable: true })
  updated_at?: string;
}
