import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class TeamEntity {
  @Field(() => Int, { nullable: true })
  id?: number;
  @Field()
  image?: string;
  @Field()
  name?: string;
  @Field()
  role?: string;
  @Field()
  summary?: string;
  @Field({ nullable: true })
  created_at?: string;
  @Field({ nullable: true })
  updated_at?: string;
}
