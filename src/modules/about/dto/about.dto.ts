import { InputType, Int, Field } from '@nestjs/graphql';

@InputType()
export class About {
  @Field()
  summary?: string;
  @Field()
  image?: string;
  @Field({ nullable: true })
  created_at?: string;
  @Field({ nullable: true })
  updated_at?: string;
}
