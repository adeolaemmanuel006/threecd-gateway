import { Field, InputType, Int } from '@nestjs/graphql';

@InputType()
export class TeamDto {
  @Field(() => Int, { nullable: true })
  id?: number;
  @Field({nullable: true})
  image?: string;
  @Field({nullable: true})
  name?: string;
  @Field({nullable: true})
  role?: string;
  @Field({nullable: true})
  summary?: string;
  @Field({ nullable: true })
  created_at?: string;
  @Field({ nullable: true })
  updated_at?: string;
}
