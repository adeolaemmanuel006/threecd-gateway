import { Injectable } from '@nestjs/common';
import { About } from './dto/about.dto';
import { FieldValue, getFirestore } from 'firebase-admin/firestore';
import { TeamDto } from './dto/team.dto';
import utils from 'src/Utils/utils';

const { monthCat, dateIOS } = utils.Date();

@Injectable()
export class AboutService {
  private db = getFirestore();
  private About = this.db.collection('ABOUT').doc('Company');
  private Team = this.db.collection('ABOUT').doc('Team');

  async getAbout(): Promise<About> {
    return (await this.About.get()).data();
  }

  async setAbout(params: About): Promise<About> {
    this.About.set(params);
    return params;
  }

  async getTeam(): Promise<TeamDto[]> {
    const data = await (await this.Team.get()).data()['data'];
    if (data) return data;
  }

  async addTeamMember(prams: TeamDto): Promise<TeamDto[]> {
    const team = await this.Team.get();
    if (!team.exists) {
      prams.id = 0;
      prams.created_at = dateIOS;
      prams.updated_at = dateIOS;
      this.Team.set({
        data: [prams],
      });
      return (await this.Team.get()).data()['data'];
    } else {
      const data: TeamDto[] = await team.data()['data'];
      let id = (await data.length) - 1;
      id += 1;
      prams.created_at = dateIOS;
      prams.updated_at = dateIOS;
      prams.id = id;

      this.Team.update({
        data: FieldValue.arrayUnion(prams),
      });

      return (await this.Team.get()).data()['data'];
    }
  }

  async editTeam(prams: TeamDto): Promise<TeamDto> {
    const teams: TeamDto[] = await (await this.Team.get()).data()['data'];
    console.log(teams);

    teams.forEach((data, ind) => {
      if (data.id === prams.id) {
        data.image = prams.image;
        data.name = prams.name;
        data.role = prams.role;
        data.summary = prams.summary;
        data.updated_at = dateIOS;
      }
    });

    console.log(teams);

    this.Team.update({
      data: teams,
    });

    return await (await this.Team.get()).data()['data'];
  }
}
