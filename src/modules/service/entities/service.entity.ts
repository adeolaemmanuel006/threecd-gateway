import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class ServiceEntity {
  @Field({nullable: true})
  summary?: string;
  @Field({ nullable: true })
  image?: string;
  @Field({ nullable: true })
  created_at?: string;
  @Field({ nullable: true })
  updated_at?: string;
}
