import { Injectable } from '@nestjs/common';
import { FieldValue, getFirestore } from 'firebase-admin/firestore';
import utils from 'src/Utils/utils';
import { Categories } from '../projects/dto/enms';
import { Service, ServiceInput, ServiceResp } from './dto/service.dto';

const { monthCat, dateIOS } = utils.Date();

@Injectable()
export class ServiceService {
  private db = getFirestore();
  private construction = this.db.collection('SERVICE').doc(Categories.const);
  private consultant = this.db.collection('SERVICE').doc(Categories.consl);
  private contractor = this.db.collection('SERVICE').doc(Categories.conrt);

  async addConstruction(prams: Service) {
    const isDone = await this.construction.set(prams);
    if (isDone) {
      return true;
    }
  }

  async addConsultant(prams: Service) {
    const isDone = await this.consultant.set(prams);
    if (isDone) {
      return true;
    }
  }

  async addContractor(prams: Service) {
    const isDone = await this.contractor.set(prams);
    if (isDone) {
      return true;
    }
  }

  async addServices(prams: ServiceInput) {
    const { type, data } = prams;
    console.log(prams);
    
    if (type === Categories.conrt) {
      return await this.addContractor(data);
    }
    if (type === Categories.const) {
      return await this.addConstruction(data);
    }
    if (type === Categories.consl) {
      return await this.addConsultant(data);
    }
  }

  async getConstruction(): Promise<Service> {
    const data = await this.construction.get();
    return await data.data();
  }

  async getConsultant(): Promise<Service> {
    const data = await this.consultant.get();
    return await data.data();
  }

  async getContractor(): Promise<Service> {
    const data = await this.contractor.get();
    return await data.data();
  }

  async getServices(prams: ServiceResp): Promise<Service> {
    const { type } = prams;
    if (type === Categories.conrt) {
      return await this.getContractor();
    }
    if (type === Categories.const) {
      return await this.getConstruction();
    }
    if (type === Categories.consl) {
      return await this.getConsultant();
    }
  }
}
