import { InputType, Int, Field } from '@nestjs/graphql';
import { Categories } from 'src/modules/projects/dto/enms';

@InputType()
export class Service {
  @Field({nullable: true})
  summary?: string;
  @Field({nullable: true})
  image?: string;
  @Field({ nullable: true })
  created_at?: string;
  @Field({ nullable: true })
  updated_at?: string;
}
@InputType()
export class ServiceInput {
  @Field()
  type: Categories;
  @Field()
  data: Service;
}
@InputType()
export class ServiceResp {
  @Field()
  type: Categories;
}
