import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Categories } from '../projects/dto/enms';
import { Service, ServiceInput, ServiceResp } from './dto/service.dto';
import { ServiceEntity } from './entities/service.entity';
import { ServiceService } from './service.service';

@Resolver()
export class ServiceResolver {
  constructor(private serviceService: ServiceService) {}

  @Mutation(() => ServiceEntity)
  addServices(@Args('servicesInput') services: ServiceInput) {
    return this.serviceService.addServices(services);
  }

  @Query(() => ServiceEntity)
  getServices(@Args('services') services: ServiceResp): Promise<Service> {
    return this.serviceService.getServices(services);
  }
}
