import { Injectable } from '@nestjs/common';
import {
  CreateProjectInput,
  DeleteProjects,
  EditProject,
} from './dto/create-project.input';
import { UpdateProjectInput } from './dto/update-project.input';
import { FieldValue, getFirestore } from 'firebase-admin/firestore';
import { Project } from './entities/project.entity';
import { SliderDto } from './dto/slider.dto';
import utils from 'src/Utils/utils';

const { monthCat, dateIOS } = utils.Date();

@Injectable()
export class ProjectsService {
  constructor() {}
  db = getFirestore();
  private Projects = this.db.collection('PROJECTS').doc(`Data`);
  private DateCategory = this.db.collection('PROJECTS').doc('Date');

  async create(createProjectInput: CreateProjectInput): Promise<Project> {
    createProjectInput.id = 0;
    createProjectInput.created_at = dateIOS;
    createProjectInput.updated_at = dateIOS;
    createProjectInput.monthCategory = monthCat;

    const isExist = await this.DateCategory.get();
    if (!isExist.exists) {
      this.DateCategory.set({
        date: [monthCat],
      });
      this.Projects.set({
        [monthCat]: [createProjectInput],
      });
    } else {
      this.DateCategory.update({
        date: FieldValue.arrayUnion(monthCat),
      });
      this.Projects.update({
        [monthCat]: [createProjectInput],
      });
    }
    return createProjectInput;
  }

  async findAll(): Promise<Project[]> {
    const getAllDates = await (await this.DateCategory.get()).data()['date'];
    const projects = [];
    for (const x of getAllDates) {
      const data: Project = (await this.Projects.get()).data()[x];
      projects.push(data);
    }
    return projects[0];
  }

  async findOne(id: number): Promise<Project> {
    const all = await this.findAll();
    const data = all.filter((data) => {
      if (data.id === id) {
        return data;
      }
    });
    return data[0];
  }

  async update(updateProjectInput: UpdateProjectInput): Promise<Project> {
    const data = await this.Projects.get();

    if (!data.exists) {
      return await this.create(updateProjectInput);
    }

    const id = data.data()[monthCat].length ? data.data()[monthCat].length : 0;
    updateProjectInput.id = id;
    updateProjectInput.created_at = dateIOS;
    updateProjectInput.updated_at = dateIOS;
    updateProjectInput.monthCategory = monthCat
    await this.Projects.update({
      [monthCat]: FieldValue.arrayUnion(updateProjectInput),
    });

    return updateProjectInput;
  }

  async slider(): Promise<SliderDto[]> {
    const projects = await this.findAll();
    const slider: SliderDto[] = [];
    for (let x = 0; x < projects.length; x++) {
      const index = Math.floor(Math.random() * projects[x].image.length);
      slider.push({
        image: projects[x].image[index],
        header: projects[x].header,
        caption: projects[x].caption,
        category: projects[x].category,
        content: projects[x].content,
        created_at: projects[x].created_at,
        updated_at: projects[x].updated_at,
      });
    }

    const one = Math.floor(Math.random() * slider.length);
    const two = Math.floor(Math.random() * slider.length);
    const three = Math.floor(Math.random() * slider.length);
    const four = Math.floor(Math.random() * slider.length);

    return [slider[one], slider[two], slider[three], slider[four]];
  }

  async deleteProjects(id: number) {
    console.log(id);

    const projects = await this.findAll();
    console.log(projects);
    
    for (let y = 0; y < projects.length; y++) {
      if (projects[y].id === id) {
        const cat = projects[y].monthCategory;
        console.log(cat);
        projects.splice(y, 1);
        this.Projects.set({ [cat]: projects });
      }
    }
    return await this.findAll();
  }

  async editProject(payload: EditProject) {
    const { id, dataInp } = payload;
    const projects = await this.findAll();
    for (const x of id) {
      for (let y = 0; y < projects.length; y++) {
        if (projects[y].id === x) {
          // projects.splice(y, 1);
          const cat = projects[y].monthCategory;
          const project: Project[] = await (await this.Projects.get()).data()[
            cat
          ];
          project.forEach((data, ind) => {
            if (data.id === x) {
              data.image = dataInp.image;
              data.caption = dataInp.caption;
              data.category = dataInp.category;
              data.content = dataInp.content;
              data.header = dataInp.header;
              data.updated_at;
              this.Projects.set({ [cat]: project });
            }
          });
        }
      }
    }
    return await this.findAll();
  }
}
