import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Categories } from '../dto/enms';

@ObjectType()
export class Project {
  @Field(() => Int, { description: 'Projects', nullable: true })
  id?: number;
  @Field()
  category?: Categories.conrt | Categories.consl | Categories.const;
  @Field()
  header?: string;
  @Field()
  caption?: string;
  @Field()
  content?: string;
  @Field((type) => [String])
  image?: string[];
  @Field({ nullable: true })
  created_at?: string;
  @Field({ nullable: true })
  updated_at?: string;
  @Field({ nullable: true })
  monthCategory?: string;
}
