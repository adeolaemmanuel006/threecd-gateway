import { Int, Field, ObjectType } from '@nestjs/graphql';
import { Categories } from '../dto/enms';

@ObjectType()
export class SliderEntity {
  @Field(() => Int, { nullable: true })
  id?: number;
  @Field()
  category?: Categories.conrt | Categories.consl | Categories.const;
  @Field()
  header?: string;
  @Field({ nullable: true })
  caption?: string;
  @Field()
  content?: string;
  @Field()
  image?: string;
  @Field({ nullable: true })
  created_at?: string;
  @Field({ nullable: true })
  updated_at?: string;
}
