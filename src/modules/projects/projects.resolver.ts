import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { ProjectsService } from './projects.service';
import { Project } from './entities/project.entity';
import { CreateProjectInput, DeleteProjects } from './dto/create-project.input';
import { UpdateProjectInput } from './dto/update-project.input';
import { SliderEntity } from './entities/slider.entity';

@Resolver()
export class ProjectsResolver {
  constructor(private readonly projectsService: ProjectsService) {}

  @Mutation(() => Project)
  createProject(
    @Args('createProjectInput') createProjectInput: CreateProjectInput,
  ) {
    return this.projectsService.create(createProjectInput);
  }

  @Query(() => [Project], { name: 'findAllProjects' })
  findAll() {
    return this.projectsService.findAll();
  }

  @Query(() => [SliderEntity], { name: 'projectsSlider' })
  projectSlider() {
    return this.projectsService.slider();
  }

  @Query(() => Project, { name: 'findOneProject' })
  findOne(@Args('id', { type: () => Int }) id: number) {
    return this.projectsService.findOne(id);
  }

  @Mutation(() => Project)
  updateProject(
    @Args('updateProjectInput') updateProjectInput: UpdateProjectInput,
  ) {
    return this.projectsService.update(updateProjectInput);
  }

  @Mutation(() => Project)
  removeProject(@Args('id', { type: () => Int }) id: number) {
    return this.projectsService.deleteProjects(id);
  }
}
