import { InputType, Int, Field } from '@nestjs/graphql';
import { Categories } from './enms';

@InputType()
export class CreateProjectInput {
  @Field(() => Int, { description: 'Create Project', nullable: true })
  id?: number;
  @Field({ nullable: true })
  category?: Categories.conrt | Categories.consl | Categories.const;
  @Field({ nullable: true })
  header?: string;
  @Field({ nullable: true })
  caption?: string;
  @Field({ nullable: true })
  monthCategory?: string;
  @Field({ nullable: true })
  content?: string;
  @Field(() => [String])
  image?: string[];
  @Field({ nullable: true })
  created_at?: string;
  @Field({ nullable: true })
  updated_at?: string;
}

@InputType()
export class EditProjectInp {
  @Field()
  category?: Categories.conrt | Categories.consl | Categories.const;
  @Field()
  header?: string;
  @Field()
  caption?: string;
  @Field()
  content?: string;
  @Field(() => [String])
  image?: string[];
}

@InputType()
export class EditProject {
  @Field(() => Int)
  id: number[];

  @Field()
  dataInp: EditProjectInp;
}
@InputType()
export class DeleteProjects {
  @Field(() => Int)
  id: number;
}
