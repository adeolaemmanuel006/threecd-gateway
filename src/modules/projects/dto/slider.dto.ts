import { InputType, Int, Field } from '@nestjs/graphql';
import { Categories } from './enms';

@InputType()
export class SliderDto {
  @Field(() => Int, { nullable: true })
  id?: number;
  @Field()
  category?: Categories.conrt | Categories.consl | Categories.const;
  @Field()
  header?: string;
  @Field({ nullable: true })
  caption?: string;
  @Field()
  content?: string;
  @Field()
  image?: string;
  @Field({ nullable: true })
  created_at?: string;
  @Field({ nullable: true })
  updated_at?: string;
}
