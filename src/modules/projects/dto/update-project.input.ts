import { CreateProjectInput } from './create-project.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';
import { Categories } from './enms';

@InputType()
export class UpdateProjectInput {
  @Field(() => Int, { nullable: true })
  id?: number;
  @Field()
  category?: Categories.conrt | Categories.consl | Categories.const;
  @Field()
  header?: string;
  @Field()
  caption?: string;
  @Field()
  content?: string;
  @Field((type) => [String])
  image?: string[];
  @Field({ nullable: true })
  created_at?: string;
  @Field({ nullable: true })
  updated_at?: string;
  @Field({ nullable: true })
  monthCategory?: string;
}
