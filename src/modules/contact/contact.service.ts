import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { ContactDto } from './dto/contact.dto';
import { FieldValue, getFirestore } from 'firebase-admin/firestore';
import utils from 'src/Utils/utils';

@Injectable()
export class ContactService {
  constructor(private mailService: MailerService) {}

  private db = getFirestore();
  private Contact = this.db.collection('CONTACT');
  private contactEmails = this.db.collection('CONTACT').doc('Emails');
  private Socials = this.db.collection('Socials');

  async contact(prams: ContactDto) {
    const response = await this.mailService.sendMail({
      to: prams.email, // list of receivers
      from: 'company-email', // sender address
      subject: prams.subject, // Subject line
      html: '<b>welcome</b>', // HTML body content
    });

    if (response) {
      const data = await (await this.contactEmails.get()).data()['emails'];
      if (!data) {
        this.contactEmails.set({ emails: [prams.email] });
      } else {
        this.contactEmails.update({
          emails: FieldValue.arrayUnion(prams.email),
        });
      }
      this.Contact.doc(prams.email).set(prams);
    }
  }

  async socials() {}
}
