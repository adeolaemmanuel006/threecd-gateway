import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class ContactDto {
  @Field()
  firstname: string;
  @Field()
  lastname: string;
  @Field()
  email: string;
  @Field()
  subject: string;
  @Field()
  message: string;
}
