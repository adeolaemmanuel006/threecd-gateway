import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class ContactDto {
  @Field()
  firstname: string;
  @Field()
  lastname: string;
  @Field()
  email: string;
  @Field()
  subject: string;
  @Field()
  message: string;
}
