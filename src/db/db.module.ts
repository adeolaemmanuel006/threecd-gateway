import { DynamicModule, Module } from '@nestjs/common';
import { DbService } from './db.service';

@Module({})
export class DbModule {
  static forRoot(config?: any): DynamicModule {
    return {
      module: DbModule,
      providers: [DbService],
      exports: [DbService],
    };
  }
}
