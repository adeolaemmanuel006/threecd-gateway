import { Injectable } from '@nestjs/common';
import { initializeApp, cert } from 'firebase-admin/app';
import { getFirestore } from 'firebase-admin/firestore';
const certificate = require('../../threecdimensionltd-cefcc-firebase-adminsdk-uftns-71a0ddf7de.json');

@Injectable()
export class DbService {
  constructor() {
    const app = initializeApp({
      credential: cert(certificate),
      databaseURL: 'https://threecd-3700e.firebaseapp.com',
    });
    console.log(app.options);
    
  }
}
