import { DynamicModule, Module } from '@nestjs/common';
import { EnvService } from './env.service';

export interface ConfigModuleOptions {
  folder: string;
}

@Module({
  providers: [EnvService],
})
export class EnvModule {
  static register(options: ConfigModuleOptions): DynamicModule {
    return {
      module: EnvModule,
      providers: [{ provide: 'OPTIONS', useValue: options }, EnvService],
      exports: [EnvService],
    };
  }
}
